require("dotenv").config;
const express = require("express");
const morgan = require("morgan");

const path = require("path");
const bodyParser = require("body-parser")
const api = require("./api");

const app = express();
const port = 3000;

app.use(
	morgan("dev"),

	bodyParser.json({extended: false}),
	bodyParser.urlencoded({extended: false}),

	express.static("public"),
	express.static("dist"),

	"/api",
	api
);

app.get("/", (req, res) => {
	res.sendFile(path.join(__dirname, "../public/index.html"))
});

app.get("/", (req, res) => {
	res.sendFile(path.join(__dirname, "../public/table.html"))
});

app.listen(port, console.log(`listening on port ${port}`));  