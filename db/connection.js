const { MongoClient, ServerApiVersion  } = require("mongodb");

const uri = "mongodb+srv://shevanAdmin:123@atlascluster.puqy5ys.mongodb.net/?retryWrites=true&w=majority&appName=AtlasCluster";

const mongoClient = new MongoClient(uri,{
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true,
  }
});

//Just a test

async function run() {
  try {
    // Connect the client to the server	(optional starting in v4.7)
    await mongoClient.connect();
    // Send a ping to confirm a successful connection
    await mongoClient.db("admin").command({ ping: 1 });
    console.log("Pinged your deployment. You successfully connected to MongoDB!");

		await listDatabases(mongoClient);

		await createNewGambler(mongoClient, {
			first_name: "John",
			last_name: "Doe",
			email_address: "john@doe.com",
			password: 123456
		})

  } catch(e){
		console.error(e);
	} finally {
    // Ensures that the client will close when you finish/error
    await mongoClient.close();
  }
}

run().catch(console.dir);

async function listDatabases(mongoClient){
	const databasesList = await mongoClient.db().admin().listDatabases();

	console.log("Databases:");
	databasesList.databases.forEach(db => {
		console.log(`- ${db.name}`);
	});
}


async function createNewGambler(mongoClient, newUser){
	const result = await mongoClient.db("Techwise").collection("gamblers").insertOne(newUser);

	console.log(`New user was created with the following id: ${result._id}`);
}

module.exports = mongoClient;