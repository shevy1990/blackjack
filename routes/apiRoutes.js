const { Router } = require("express");
const userRouter = Router();
const apiRouter = Router();
const mongoClient = require("../db/connection");
const ObjectId = require("mongodb").ObjectId;
const db = mongoClient.db("TechWise");

userRouter.get("/all", async (req, res) => {
  try {
    const users = db.collection("gamblers");
    const allUsers = await users.find({}).toArray();
    res.json(allUsers);
		console.log(allUsers);
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

userRouter.get("/first", async (req, res) => {
  try {
    const users = db.collection("gamblers");
    const firstUser = await users.findOne({});
    res.json(firstUser);
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

userRouter.get("/:id", async (req, res) => {
  console.log(req.params);
  try {
    const users = db.collection("gamblers");
    const searchedUser = await users.findOne({
      _id: new ObjectId(req.params.id),
    });
    res.json(searchedUser);
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

userRouter.post("/", async (req, res) => {
  console.log(req.body);
  try {
    const users = db.collection("gamblers");
    const newUser = await users.insertOne({
      ...req.body,
      balance: 6789,
      assets: [],
    });
    res.json(newUser);
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

userRouter.put("/:id", async (req, res) => {
  console.log(req.params);
  try {
    const users = db.collection("gamblers");
    const searchedUser = await users.replaceOne(
      {
        _id: new ObjectId(req.params.id),
      },
      req.body
    );
    res.json(searchedUser);
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

userRouter.patch("/:id", async (req, res) => {
  console.log(req.params);
  console.log(req.body);
  try {
    const users = db.collection("gamblers");
    const searchedUser = await users.updateOne(
      { _id: new ObjectId(req.params.id) },
      { $set: { ...req.body } }
    );
    res.json(searchedUser);
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

userRouter.delete("/:id", async (req, res) => {
  console.log(req.params);
  try {
    const users = db.collection("gamblers");
    const searchedUser = await users.deleteOne({
      _id: new ObjectId(req.params.id),
    });
    res.json(searchedUser);
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

apiRouter.use("/user", userRouter);

module.exports = apiRouter;
